# Examples for the Open IoT SDK

This repository hosts examples that are generated from templates in [the Open IoT SDK](https://gitlab.arm.com/iot/open-iot-sdk/sdk).

To view, build and run any example, navigate to the directory of the example of your choice and follow the `README.md`
there.

**Note**: This repository is only updated by an automation bot to regularly synchronize examples generated from the
latest Open IoT SDK. If you would like to contribute to any examples, please add your changes to templates in _the Open
IoT SDK_ and raise a merge request there.

## License and contributions

The software is provided under the Apache-2.0 license. All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). Open IoT SDK may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the LICENSE file.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the Open IoT SDK.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).
