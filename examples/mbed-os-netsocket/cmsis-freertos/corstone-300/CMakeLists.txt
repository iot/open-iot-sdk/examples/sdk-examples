# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.21.0 FATAL_ERROR)

# Set config variables for the project and dependencies
set(CMAKE_SYSTEM_PROCESSOR cortex-m55)
set(IOTSDK_FETCH_LIST
    cmsis-sockets-api
    lwip
    mbedtls
    pigweed
    cmsis-freertos
    cmsis-5
)
set(CMSIS_PACK_PLATFORM_DEVICE_NAME   "SSE-300-MPS3")


include(FetchContent)

set(pw_log_BACKEND pw_log_cmsis_driver CACHE STRING "" FORCE)

option(MBED_CONF_APP_USE_TLS_SOCKET "Use TLS socket for connection" OFF)

if (MBED_CONF_APP_USE_TLS_SOCKET)
  add_definitions(-DMBED_CONF_APP_USE_TLS_SOCKET=ON)
endif()

# Toolchain files need to exist before first call to project
FetchContent_Declare(iotsdk-toolchains
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/toolchain.git
    GIT_TAG         d600b9dc9aca55c8428b63b9b8eff1ff97743382
    SOURCE_DIR      ${CMAKE_BINARY_DIR}/toolchains
)
FetchContent_MakeAvailable(iotsdk-toolchains)

# Declare top-level project
project(iotsdk-mbed-os-netsocket)

# Set executable suffix to be toolchain-independent for ease of documentation
# and testing. This needs to be done after first call to project().
set(CMAKE_EXECUTABLE_SUFFIX .elf)

# Add Open IoT SDK
FetchContent_Declare(open-iot-sdk
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git
    GIT_TAG         3829c52523d9fddc0af1e097e16e772377075b99

)
FetchContent_MakeAvailable(open-iot-sdk)

# Add Open IoT SDK modules to path
list(APPEND CMAKE_MODULE_PATH ${open-iot-sdk_SOURCE_DIR}/cmake)

# Add the project_options tool for code quality checks on this example.
# In a user's own project, the use of project_options is entirely optional.
FetchContent_Declare(project_options
    GIT_REPOSITORY  https://github.com/cpp-best-practices/project_options.git
    GIT_TAG         v0.21.0
)
FetchContent_MakeAvailable(project_options)
include(${project_options_SOURCE_DIR}/Index.cmake)
include(${project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

# Apply Open IoT SDK's default compiler warnings to this example via project_options
include(${open-iot-sdk_SOURCE_DIR}/cmake/AddToolchainWarningFlags.cmake)
set(COMPILER_WARNINGS ${IOTSDK_WARNING_FLAGS} ${IOTSDK_WARNING_AS_ERRORS_FLAGS})
dynamic_project_options(
    CLANG_WARNINGS  ${COMPILER_WARNINGS}
    GCC_WARNINGS    ${COMPILER_WARNINGS}
)

include(ConvertElfToBin)
include(CTest)

include(cmsis-pack-platform.cmake)

# Configure component properties

target_compile_definitions(iotsdk-cmsis-core-device
    INTERFACE
        OS_PRIVILEGE_MODE=1
)

# Add RTOS configuration headers
target_include_directories(cmsis-config INTERFACE cmsis-config)
target_include_directories(freertos-config INTERFACE freertos-config)
target_link_libraries(freertos-config INTERFACE cmsis-config)

# Link cmsis-rtos-api against a concrete implementation
target_link_libraries(cmsis-rtos-api
    PUBLIC
        freertos-cmsis-rtos
    freertos-kernel-heap-4
)

# lwipcore requires the config defined by lwip-cmsis-port
target_link_libraries(lwipcore
    PRIVATE
        lwip-cmsis-port
)

# lwip requires user_lwipopts.h, we use the template provided by the lwip-cmsis-port
get_target_property(lwip-cmsis-port_SOURCE_DIR lwip-cmsis-port SOURCE_DIR)

target_include_directories(lwipopts
    INTERFACE
        ${lwip-cmsis-port_SOURCE_DIR}/include/user-config
)

# provide method to use for tracing by the lwip port (optional)
target_compile_definitions(lwipopts
    INTERFACE
        DEBUG_PRINT=printf
)

# mbedtls config
target_include_directories(mbedtls-config
    INTERFACE
        mbedtls-config
)

target_link_libraries(mbedtls-config
    INTERFACE
        mbedtls-threading-cmsis-rtos
)

target_compile_definitions(mbedtls-config
    INTERFACE
        MBEDTLS_CONFIG_FILE="mbedtls_config.h"
)

# Pigweed
target_link_libraries(pw_log_cmsis_driver
    PRIVATE
    pw_log_cmsis_driver_rtos_lock
)

# Link serial retargeting with a device-specific UART CMSIS-Driver.
target_link_libraries(iotsdk-serial-retarget PRIVATE cmsis-pack-platform)

# Add user application
add_executable(iotsdk-mbed-os-netsocket main.cpp)

target_link_libraries(iotsdk-mbed-os-netsocket
    # Startup
    cmsis-pack-platform

    # lwip and IP
    iotsdk-ip-network-api
    lwip-sockets
    lwipopts

    mbedtls

    # Pigweed
    pw_log
    pw_log_cmsis_driver
    pw_log_cmsis_driver_rtos_lock

    # Factories
    iotsdk-serial-retarget

    # SDK project options/warnings (this is optional)
    project_options
    project_warnings

    # Mbed OS netsocket
    mbed-os-netsocket
)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/gcc.ld)
    target_link_options(iotsdk-mbed-os-netsocket PRIVATE -T ${linker_script})
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/armclang.sct)
    target_link_options(iotsdk-mbed-os-netsocket PRIVATE --scatter=${linker_script})
endif()
set_target_properties(iotsdk-mbed-os-netsocket PROPERTIES LINK_DEPENDS ${linker_script})

target_elf_to_bin(iotsdk-mbed-os-netsocket)

add_test(
    NAME    iotsdk-mbed-os-netsocket
    COMMAND htrun
        --image-path=iotsdk-mbed-os-netsocket.elf
        --compare-log=${CMAKE_CURRENT_LIST_DIR}/test.log
        --sync=0 --baud-rate=115200 --polling-timeout=240
        --micro=FVP_CS300_U55 --fm=MPS3
    COMMAND_EXPAND_LISTS
)
