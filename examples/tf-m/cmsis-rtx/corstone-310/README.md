# Trusted Firmware-M (TF-M) example

This example shows the usage of Trusted Firmware-M (TF-M). RTOS: CMSIS RTX, platform: Corstone-310.

## Building the example

### Prerequisites

Please refer to [prerequisites for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md).

### Background

#### CMake build process

Building a CMake project involves two steps:
* Configuration: What to configure depends on the project. For the Open IoT SDK,
this includes build directory, toolchain selection, components to fetch, options
for examples and tests, etc. This step creates and populates the build directory
with everything needed for building, including automatically fetching components.
* Building: Triggering a build using the configuration set up in the
Configuration step.

#### Selecting the toolchain

To configure CMake to use a particular toolchain you must pass in a
[toolchain file](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html#cross-compiling).

The example will automatically fetch the default toolchain files for
* Arm Compiler 6: `--toolchain toolchains/toolchain-armclang.cmake`
* GNU Arm Embedded Toolchain (GCC) `--toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake`

### Configuration and building

To configure this example, e.g. using GCC:

```
cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
```

**Notes**:
* If you generated this example from a copy of the Open IoT SDK that contains *your own modifications*, then you
**must** append `-D FETCHCONTENT_SOURCE_DIR_OPEN-IOT-SDK=<YOUR_LOCAL_SDK>` (note the difference between `_` and `-`) to
the command above, where `<YOUR_LOCAL_SDK>` is the absolute or relative path to the SDK repository located on your hard
drive.

To build:

```
cmake --build __build
```

## Running the example

Once the example application has been built, it can be found in .elf and .bin formats inside the `__build` directory.

For information on setting up Arm Virtual Hardware through AWS on an [Amazon Machine Image] (AMI), please see [here][Setting Up AVH].

### To run it on an Arm Virtual Hardware (AVH) target:

#### PSA Crypto API
```sh
VHT_Corstone_SSE-310 -a __build/iotsdk-example-tf-m-crypto_merged.elf
```
#### PSA Firmware Update API
```sh
VHT_Corstone_SSE-310 -a __build/iotsdk-example-tf-m-firmware-update_merged.elf
```


### To run it on a Fixed Virtual Platform (FVP) target:

**Note A standalone Arm Ecosystem FVP is not available for Corstone-310, only the version included in the Keil MDK**#### PSA Crypto API
```sh
FVP_Corstone_SSE-310 -a __build/iotsdk-example-tf-m-crypto_merged.elf
```
#### PSA Firmware Update API
```sh
FVP_Corstone_SSE-310 -a __build/iotsdk-example-tf-m-firmware-update_merged.elf
```




## Troubleshooting

### Windows path issue

When running the above `cmake -B` command on Windows, you may run into the following issue:

```
ninja: error: Stat(__build/_deps/a-very-very-very-very-very-long-component-name-subbuild/a-very-very-very-very-very-long-component-name-populate-prefix/src/a-very-very-very-very-very-long-component-name-populate-stamp/a-very-very-very-very-very-long-component-name-populate-done): Filename longer than 260 characters
```

You can work around the error by performing either of the following solutions:
- using WSL
- moving the project closer to the drive's root
- using `subst` windows command

For example `subst` to use new drive `z:` if the path to your working directory is `c:\foo\very-very-long-directory-name\my-working-directory`,
run the commands as follows:
```
subst z: c:\foo\very-very-long-directory-name\my-working-directory
z:
git config --system core.longpaths true
```

## Further information

Please refer to [documentation for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/README.md).

[Setting Up AVH]: https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/running_apps_on_avh.md
[Amazon Machine Image]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html
