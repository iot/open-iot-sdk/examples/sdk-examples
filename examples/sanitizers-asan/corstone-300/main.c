/*
 *Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 *SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_USART.h"

#include <inttypes.h>
#include <stdlib.h>

#define PW_LOG_MODULE_NAME "main"
#include "pw_log/log.h"

extern void Test1();
extern int Test2();

static ARM_DRIVER_USART *serial = NULL;
extern ARM_DRIVER_USART *get_example_serial();

static void serial_setup(void)
{
    serial = get_example_serial();
    if ((serial->Initialize(NULL) != ARM_DRIVER_OK) || (serial->PowerControl(ARM_POWER_FULL) != ARM_DRIVER_OK)
        || (serial->Control(ARM_USART_MODE_ASYNCHRONOUS, 115200) != ARM_DRIVER_OK)) {
        return;
    }
    /* Some drivers have TX and RX enabled by default and lacks option to enable/disable them. */
    int ret = serial->Control(ARM_USART_CONTROL_TX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
    ret = serial->Control(ARM_USART_CONTROL_RX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
}

int main(void)
{
    serial_setup();

    pw_log_cmsis_driver_init(serial);

    PW_LOG_INFO("Starting tests");

    Test1();
    Test2();

    PW_LOG_INFO("Done");
    return 0;
}
