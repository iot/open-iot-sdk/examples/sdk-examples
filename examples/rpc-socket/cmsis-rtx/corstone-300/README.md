# IoT Socket Example

## Overview

The RPC server running on the board provides an RPC API which wraps a subset of the CMSIS IoT Socket API.

A summary of the IoT Socket API can be found
[here](https:/mdk-packs.github.io/IoT_Socket/html/group__iotSocketAPI.html#ga22311af9784ef6317ce092ccf392d049).

Each IoT Socket API function is wrapped by one endpoint in the RPC API.
The caller places the API input parameters in a protobuf request message and the protobuf API calls the IoT Socket API.
It places the status and any output parameter values into the response message and returns a Pigweed status code.
The Pigweed status should be checked first.

The example uses pyedmgr to spawn two FVPs and instructs the server to open a socket on each FVP, with one acting as a TCP server and the other as a client.

### Limitations

* Messages have size limitations not present in the API, check `src/iot-socket.options` for maximum sizes of fields
* Protobuf does not have 16-bit integers, so ports are transmitted as uint32 instead

## Prerequisites

You can fulfill the following requirements by sourcing the `bootstrap.sh` script that comes with the example.

* The Protocol Buffers compiler `protoc` version 3.19.6 is installed and in `PATH`
* The `PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python` environment variable is set
* The Pigweed and Pyedmgr libraries and dependencies are installed

You must fulfill these requirements separately:

* An FVP must be installed with its binary in `PATH`
* The FVP either came with Iris or Iris' location is in `PYTHONPATH`

```sh
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION="python"

```

The `bootstrap.sh` script will download the `protoc` compiler release, unpack it and add it to the $PATH
and set the `PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION`.

## Build

As with other exampels we use `cmake` to build. Before doing so, in a new terminal session, we must activate the example's environment by sourcing `bootstrap.sh`.

```sh
source `example/bootstrap.sh` # If in a fresh terminal session
cmake -S. -B__build -GNinja --toolchain=toolchains/toolchain-arm-none-eabi-gcc.cmake
```


## Run

### Setup

First bring up the network as root:

```sh
sudo example/network_setup.sh up
```

Then run the test, either with `ctest` or manually

### With `ctest`

```sh
ctest
```

### Manually

```sh
example/run-example FVP_Corstone_SSE-300_Ethos-U55 __build/_deps/pyedmgr-src __build/iotsdk-example-rpc-socket
```

### Cleanup

After running the example we should restore the network to its prior state:

```sh
sudo example/network_setup.sh down
```
