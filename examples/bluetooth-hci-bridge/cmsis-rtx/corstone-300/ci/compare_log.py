#!/usr/bin/env python3

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

import sys


def main():
    if len(sys.argv) != 3:
        print("usage :")
        print("  {} log_file expected_output_file".format(sys.argv[0]))
        exit(1)

    match = False
    with open(sys.argv[1]) as log_file, open(sys.argv[2]) as expected_output_file:
        match = True
        log_file_iter = iter(log_file)
        for expected_line in expected_output_file:
            try:
                while True:
                    log_line = next(log_file_iter)
                    if log_line.find(expected_line) != -1:
                        break
            except (StopIteration):
                # we are still expecting some lines, yet we reach EOF for the log
                match = False
                break
    if match:
        exit(0)
    else:
        exit(1)


main()
