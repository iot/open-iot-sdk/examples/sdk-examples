# Bluetooth HCI bridge example

This example uses a Linux Bluetooth controller external to the FVP connected to it through a loopback socket bridge.
This example requires the use of AVH FVP to provide virtual sockets.

The application allows the Bluetooth host running inside the FVP to connect to a simulated controller running in Babblesim.

## Babblesim

For the example to work, you need to provide a set of applications running on the host running the FVP.
The FVP example will attempt to connect to a relay running on the host using a loopback IP socket.

The applications are in the babblesim-tools repository:
https://git.gitlab.arm.com/iot/open-iot-sdk/libraries/tools-for-babble-sim.git

Please read the README.md in the babblesim-tools repo for details on how to build and run
all the elements needed for the example to successfully connect to the controller.

Once babblesim and the relay are running you may proceed with launching the FVP example:

```sh
VHT_Corstone_SSE-310 -a __build/iotsdk-example-bluetooth-hci-bridge.elf
```

*Note: **It is not possible to run this example using an Ecosystem FVP. Please use the FVP available through AVH.**

This will run the BLE stack and attach it to the hci-bridge and all the way to babblesim. In another terminal, you may now connect something else on the second HCI interface created in BlueZ, or you may observe the advertising directly using

```sh
sudo btmon
```

### Set up Docker

Integrating this test in a CI can be hard because dbus (one requirement of BlueZ) doesn't work properly in a docker container. One may still run the test manually or in a virtual machine using the scripts in `ci/`

If you have access to the FVP in a container, you may start it with `ci/start_manual_docker.sh`. You'll need to set some environment variables first: The docker registry in CONTAINER_IMAGE_REGITRY and the image name in CONTAINER_IMAGE_NAME. You may also give your instance a name through CONTAINER_NAME

### Set up AMI

Otherwise, you'll have to use an [Amazon Machine Image (AMI)](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html) that contain the FVP like **Arm Virtual Hardware By Arm | Version 1.2.3**

The AMI currently don't ship with bluetooth installed (`modprobe bluetooth` will fail) so you will need to swap the kernel for another one with bluetooth build in.

```sh
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15/amd64/linux-headers-5.15.0-051500_5.15.0-051500.202110312130_all.deb
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15/amd64/linux-image-unsigned-5.15.0-051500-generic_5.15.0-051500.202110312130_amd64.deb
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15/amd64/linux-modules-5.15.0-051500-generic_5.15.0-051500.202110312130_amd64.deb
sudo dpkg -i *.deb
sudo update-grub
```
Check that the new kernel (5.15) is found and listed first in the list.
```sh
grep "menuentry " /boot/grub/grub.cfg | cut -f 2 -d "'" | nl -v 0
```
Then reboot.
```sh
sudo reboot
```
You should be able to start the bluetooth service.
```sh
sudo modprobe bluetooth
```

Once the bluetooth is running (`service bluetooth status`), you may set up the environment with
```sh
source ci/setup_environment.sh
```
This will install the apt dependencies, babblesim, and the other tools required.
This should only be needed once. For future runs, you'll just have to add the babblesim install dir to PATH if it's not there already. By default you can do it with:
```sh
export PATH=~/babblesim-tools/scripts/:${PATH}
```


## Building the example

### Prerequisites

Please refer to [prerequisites for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md).

### Background

#### CMake build process

Building a CMake project involves two steps:
* Configuration: What to configure depends on the project. For the Open IoT SDK,
this includes build directory, toolchain selection, components to fetch, options
for examples and tests, etc. This step creates and populates the build directory
with everything needed for building, including automatically fetching components.
* Building: Triggering a build using the configuration set up in the
Configuration step.

#### Selecting the toolchain

To configure CMake to use a particular toolchain you must pass in a
[toolchain file](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html#cross-compiling).

The example will automatically fetch the default toolchain files for
* Arm Compiler 6: `--toolchain toolchains/toolchain-armclang.cmake`
* GNU Arm Embedded Toolchain (GCC) `--toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake`

### Configuration and building

To configure this example, e.g. using GCC:

```
cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
```

**Notes**:
* If you generated this example from a copy of the Open IoT SDK that contains *your own modifications*, then you
**must** append `-D FETCHCONTENT_SOURCE_DIR_OPEN-IOT-SDK=<YOUR_LOCAL_SDK>` (note the difference between `_` and `-`) to
the command above, where `<YOUR_LOCAL_SDK>` is the absolute or relative path to the SDK repository located on your hard
drive.

To build:

```
cmake --build __build
```

## Running the example

Once the example application has been built, it can be found in .elf and .bin formats inside the `__build` directory.

For information on setting up Arm Virtual Hardware through AWS on an [Amazon Machine Image] (AMI), please see [here][Setting Up AVH].

### To run it on an Arm Virtual Hardware (AVH) target:

```sh
VHT_Corstone_SSE-310 -a __build/iotsdk-example-bluetooth-hci-bridge.elf
```

### To run it on a Fixed Virtual Platform (FVP) target:

*Note: **It is not possible to run this example using an Ecosystem FVP. Please use the FVP available through AVH.**

### Running the test

Start the python virtual environment
```sh
source venv/bin/activate
```
And run the test
```sh
ci/test.sh
```
If you run the test in a slow machine, you may need to edit `ci/test_requester.py` to increase the timeout value.


## Troubleshooting

### Windows path issue

When running the above `cmake -B` command on Windows, you may run into the following issue:

```
ninja: error: Stat(__build/_deps/a-very-very-very-very-very-long-component-name-subbuild/a-very-very-very-very-very-long-component-name-populate-prefix/src/a-very-very-very-very-very-long-component-name-populate-stamp/a-very-very-very-very-very-long-component-name-populate-done): Filename longer than 260 characters
```

You can work around the error by performing either of the following solutions:
- using WSL
- moving the project closer to the drive's root
- using `subst` windows command

For example `subst` to use new drive `z:` if the path to your working directory is `c:\foo\very-very-long-directory-name\my-working-directory`,
run the commands as follows:
```
subst z: c:\foo\very-very-long-directory-name\my-working-directory
z:
git config --system core.longpaths true
```

## Further information

Please refer to [documentation for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/README.md).

[Setting Up AVH]: https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/running_apps_on_avh.md
[Amazon Machine Image]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html
