# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

import asyncio
import sys
from bleak import BleakClient
from bleak import BleakScanner

EXAMPLE_UUID = "5c3a659e-897e-45e1-b016-007107c96df6"
TIMEOUT = 15.0

adapter = "hci" + sys.argv[1]


def on_disconnect(client):
    print("disconnected")


async def main():
    device = await BleakScanner.find_device_by_filter(
        lambda d, ad: d.name and d.name.lower() == "mynewt",
        adapter=adapter,
        timeout=TIMEOUT,
    )

    if device is None:
        print("No mynewt device found")
        return
    else:
        print("mynewt device found")

    async with BleakClient(
        device, on_disconnect, adapter=adapter, timeout=TIMEOUT
    ) as client:
        char = await client.read_gatt_char(EXAMPLE_UUID)
        print("char value: {0}".format("".join(map(hex, char))))
        await client.disconnect()


asyncio.run(main())
