# Copyright (c) 2021-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.21.0 FATAL_ERROR)

# Set config variables for the project and dependencies
set(CMAKE_SYSTEM_PROCESSOR cortex-m85)
set(IOTSDK_FETCH_LIST
    cmsis-freertos
    cmsis-5
    pigweed
    mynewt-nimble
    cmsis-sockets-api
    avh
    pigweed
)
set(CMSIS_PACK_PLATFORM_DEVICE_NAME   "SSE-310-MPS3_FVP")


include(FetchContent)

# Toolchain files need to exist before first call to project
FetchContent_Declare(iotsdk-toolchains
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/toolchain.git
    GIT_TAG         d600b9dc9aca55c8428b63b9b8eff1ff97743382
    SOURCE_DIR      ${CMAKE_BINARY_DIR}/toolchains
)
FetchContent_MakeAvailable(iotsdk-toolchains)

# Declare top-level project
project(iotsdk-example-bluetooth-hci-bridge)

# Set executable suffix to be toolchain-independent for ease of documentation
# and testing. This needs to be done after first call to project().
set(CMAKE_EXECUTABLE_SUFFIX .elf)

# Set Pigweed backends before SDK fetch.
function(set_pw_backend FACADE BACKEND)
    set(${FACADE}_BACKEND ${BACKEND} CACHE STRING "" FORCE)
endfunction()
set_pw_backend(pw_log pw_log_cmsis_driver)

# Add Open IoT SDK
FetchContent_Declare(open-iot-sdk
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git
    GIT_TAG         3829c52523d9fddc0af1e097e16e772377075b99

)
FetchContent_MakeAvailable(open-iot-sdk)

# Add Open IoT SDK modules to path
list(APPEND CMAKE_MODULE_PATH ${open-iot-sdk_SOURCE_DIR}/cmake)

# Add the project_options tool for code quality checks on this example.
# In a user's own project, the use of project_options is entirely optional.
FetchContent_Declare(project_options
    GIT_REPOSITORY  https://github.com/cpp-best-practices/project_options.git
    GIT_TAG         v0.21.0
)
FetchContent_MakeAvailable(project_options)
include(${project_options_SOURCE_DIR}/Index.cmake)
include(${project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

# Apply Open IoT SDK's default compiler warnings to this example via project_options
include(${open-iot-sdk_SOURCE_DIR}/cmake/AddToolchainWarningFlags.cmake)
set(COMPILER_WARNINGS ${IOTSDK_WARNING_FLAGS} ${IOTSDK_WARNING_AS_ERRORS_FLAGS})
dynamic_project_options(
    CLANG_WARNINGS  ${COMPILER_WARNINGS}
    GCC_WARNINGS    ${COMPILER_WARNINGS}
)

include(ConvertElfToBin)
include(CTest)

include(cmsis-pack-platform.cmake)

# Configure component properties

target_compile_definitions(iotsdk-cmsis-core-device
    INTERFACE
        OS_PRIVILEGE_MODE=1
)

# Add RTOS configuration headers
target_include_directories(cmsis-config INTERFACE cmsis-config)
target_include_directories(freertos-config INTERFACE freertos-config)
target_link_libraries(freertos-config INTERFACE cmsis-config)

# Link cmsis-rtos-api against a concrete implementation
target_link_libraries(cmsis-rtos-api
    PUBLIC
        freertos-cmsis-rtos
    freertos-kernel-heap-4
)

# select nimble implementation
target_link_libraries(cmsis-npl
    PUBLIC
        nimble-pw-logging
)

target_sources(nimble-user-config INTERFACE "nimble-user-config.h")
target_compile_definitions(nimble-user-config INTERFACE NIMBLE_USER_CONFIG_PATH="nimble-user-config.h")
target_include_directories(nimble-user-config INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(mynewt-nimble
    PUBLIC
        cmsis-nimble-port
    PRIVATE
        mynewt-nimble-tinycrypt
)
target_link_libraries(cmsis-nimble-port
    PUBLIC
        iot-hci-host-nimble
)
target_link_libraries(iot-hci-host-nimble
    PUBLIC
        iot-hci-controller-ip-socket
)
target_link_libraries(iot-hci-controller-ip-socket
    PRIVATE
        avh-sockets
)

# Link serial retargeting with a device-specific UART CMSIS-Driver.
target_link_libraries(iotsdk-serial-retarget PRIVATE cmsis-pack-platform)

# Add example application
add_executable(iotsdk-example-bluetooth-hci-bridge main.c)

target_link_libraries(iotsdk-example-bluetooth-hci-bridge
    iot-hci
    mynewt-nimble

    pw_log
    pw_log_cmsis_driver
    pw_log_null_lock

    iotsdk-serial-retarget

    pw_log
    pw_log_cmsis_driver_rtos_lock

    freertos-cmsis-rtos
    freertos-kernel-heap-4
    cmsis-pack-platform

    # SDK project options/warnings (this is optional)
    project_options
    project_warnings
)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/gcc.ld)
    target_link_options(iotsdk-example-bluetooth-hci-bridge PRIVATE -T ${linker_script})
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/armclang.sct)
    target_link_options(iotsdk-example-bluetooth-hci-bridge PRIVATE --scatter=${linker_script})
endif()
set_target_properties(iotsdk-example-bluetooth-hci-bridge PROPERTIES LINK_DEPENDS ${linker_script})
target_elf_to_bin(iotsdk-example-bluetooth-hci-bridge)

add_test(
    NAME    iotsdk-example-bluetooth-hci-bridge
    WORKING_DIRECTORY /builds/workspace
    COMMAND ./templates/examples/bluetooth-hci-bridge/ci/setup_test.sh
)
