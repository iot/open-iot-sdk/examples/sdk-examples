# Example for AWS Client

This example demonstrates the use of the `coreMQTT` library from the AWS IOT SDK.

The example connects to the AWS server, publish the message `Hello World!` in the
MQTT topic `OpenIoTSDK/demo`. It wait for the acknowledgement from the server and
release the connection.

The network management part is provided by the library `iot-sdk-aws-network-manager`
which establish the TCP connection to the AWS server then encrypts it. This library
is built on top of other building blocks present in this SDK.

## Configuration

AWS credentials must be filled in to access the AWS service.
These credentials are displayed in the files:
- [aws-credentials/aws_clientcredential.h](aws-credentials/aws_clientcredential.h)
- [aws-credentials/aws_clientcredential_keys.h](aws-credentials/aws_clientcredential_keys.h)

The application specifically requires:
- `clientcredentialMQTT_BROKER_ENDPOINT`: fully-qualified DNS name of your MQTT broker
- `clientcredentialMQTT_BROKER_PORT`: MQTT broker port
- `keyCA1_ROOT_CERTIFICATE_PEM`: AWS root certificate
- `keyCLIENT_CERTIFICATE_PEM`: PEM-encoded client certificate.
- `keyCLIENT_PRIVATE_KEY_PEM`: PEM-encoded client private key.
- `keyCLIENT_PUBLIC_KEY_PEM`: PEM-encoded client public key.
- `clientcredentialIOT_THING_NAME`: Name of the IoT thing.


## Building the example

### Prerequisites

Please refer to [prerequisites for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md).

### Background

#### CMake build process

Building a CMake project involves two steps:
* Configuration: What to configure depends on the project. For the Open IoT SDK,
this includes build directory, toolchain selection, components to fetch, options
for examples and tests, etc. This step creates and populates the build directory
with everything needed for building, including automatically fetching components.
* Building: Triggering a build using the configuration set up in the
Configuration step.

#### Selecting the toolchain

To configure CMake to use a particular toolchain you must pass in a
[toolchain file](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html#cross-compiling).

The example will automatically fetch the default toolchain files for
* Arm Compiler 6: `--toolchain toolchains/toolchain-armclang.cmake`
* GNU Arm Embedded Toolchain (GCC) `--toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake`

### Configuration and building

To configure this example, e.g. using GCC:

```
cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
```

**Notes**:
* If you generated this example from a copy of the Open IoT SDK that contains *your own modifications*, then you
**must** append `-D FETCHCONTENT_SOURCE_DIR_OPEN-IOT-SDK=<YOUR_LOCAL_SDK>` (note the difference between `_` and `-`) to
the command above, where `<YOUR_LOCAL_SDK>` is the absolute or relative path to the SDK repository located on your hard
drive.

To build:

```
cmake --build __build
```

## Running the example

Once the example application has been built, it can be found in .elf and .bin formats inside the `__build` directory.

For information on setting up Arm Virtual Hardware through AWS on an [Amazon Machine Image] (AMI), please see [here][Setting Up AVH].

### To run it on an Arm Virtual Hardware (AVH) target:

```sh
VHT_Corstone_SSE-310 -a __build/iotsdk-example-aws-client.elf -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1
```

### To run it on a Fixed Virtual Platform (FVP) target:

**Note: A standalone Arm Ecosystem FVP is not available for Corstone-310, only the version included in the Keil MDK**

```sh
FVP_Corstone_SSE-310 -a __build/iotsdk-example-aws-client.elf -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1
```



## Troubleshooting

### Windows path issue

When running the above `cmake -B` command on Windows, you may run into the following issue:

```
ninja: error: Stat(__build/_deps/a-very-very-very-very-very-long-component-name-subbuild/a-very-very-very-very-very-long-component-name-populate-prefix/src/a-very-very-very-very-very-long-component-name-populate-stamp/a-very-very-very-very-very-long-component-name-populate-done): Filename longer than 260 characters
```

You can work around the error by performing either of the following solutions:
- using WSL
- moving the project closer to the drive's root
- using `subst` windows command

For example `subst` to use new drive `z:` if the path to your working directory is `c:\foo\very-very-long-directory-name\my-working-directory`,
run the commands as follows:
```
subst z: c:\foo\very-very-long-directory-name\my-working-directory
z:
git config --system core.longpaths true
```

## Further information

Please refer to [documentation for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/README.md).

[Setting Up AVH]: https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/running_apps_on_avh.md
[Amazon Machine Image]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html
