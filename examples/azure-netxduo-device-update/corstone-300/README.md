# Azure Device Update example based on NetX Duo and PSA Firmware Update

This example uses NetX Duo's Azure IoT addon to connect to Azure IoT Hub, download an update from the Azure Device
Update service and install the update using the PSA Firmware Update API provided by TrustedFirmware-M (TF-M).

## Setting up Azure cloud resources

Set up Azure cloud resources as follows, if you (or your organization's administrator) haven't done so already.

* An IoT Hub instance. You can use an existing one, or create a new one by following [Create an IoT hub using the Azure
portal](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-create-through-portal#create-an-iot-hub).
* A Device Update instance. You can use an existing one, or create a new one by following [Create Device Update for IoT
Hub resources](https://learn.microsoft.com/en-us/azure/iot-hub-device-update/create-device-update-account).
* Required access control roles. You can grant required roles by following [Configure access control roles for Device
Update resources](https://learn.microsoft.com/en-us/azure/iot-hub-device-update/configure-access-control-device-update).
* A storage container. You can use an existing one, or create a new one by following [Create a container](https://learn.microsoft.com/en-us/azure/storage/blobs/blob-containers-portal#create-a-container).

## Verify that you (or your organization's administrator) have set up the resources correctly

1. Open the *IoT Hub* instance you want to connect your device to.
2. Click on *Access control (IAM)* (under *Overview*) on the left, followed by the *Role assignments* tab.

    2.1. Check that either you, or a group you are a member of, have the following roles:
    * *Contributor*, or a more privileged role such as *Owner*.
    * *Device Update Administrator*.

    2.2. Check that *Azure Device Update* (App) has the *IoT Hub Data Contributor* role.
3. Click on *Updates* (under *Device management*) on the left. This should give you a few tabs: *Updates*, *Groups and
Deployments*, *Diagnostics* and *Get Started*. Click on the *Updates* tab.

    3.1 Click on *Import a new update* -> *Select from storage container*.

    3.2 Check that the storage container you want to use is on the list.

## Setting IoT Hub connection credentials in the sample

The sample connects to the device in the IoTHub using a symmetric key. To register a new device in IoTHub use the
[doc](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-create-through-portal#register-a-new-device-in-the-iot-hub)
to create a device with symmetric key authentication.

After device's registration is complete, copy the connection string
for the device with the following format **HostName=<>\\;DeviceId=<>\\;SharedAccessKey=<>**.

Edit the following macros in
`sample_azure_iot_credentials.h`:

```
#define HOST_NAME                                   "<Hostname from connection string>"
#define DEVICE_ID                                   "<DeviceId from connection string>"
#define DEVICE_SYMMETRIC_KEY                        "<SharedAccessKey from connection string>"
```
---
**Note**

The credentials are also essential to configure the application with CMake as the `DEVICE_ID` value in `sample_azure_iot_credentials.h` is used as a unique ID to create an update import manifest.

---
## Importing and deploying an update

Once you have built the example, the build directory contains
* `iotsdk-example-azure-netxduo-device-update_merged.elf`: a combined image of the bootloader, TF-M and the current
version of the example application
* `update_image.bin`: the update image,
* `update-0.0.2.importmanifest.json`: the import manifest containing metadata of the update

First, run the current version of the application. Once the application has connected to Azure IoT Hub, follow ["Import
an update to Device Update for IoT Hub"](https://learn.microsoft.com/en-us/azure/iot-hub-device-update/import-update?tabs=portal)
to import the update by uploading `update_image.bin` and `update-0.0.2.importmanifest.json`.

To deploy the update you have just imported, follow ["Deploy an update by using Device Update for Azure IoT Hub"](https://learn.microsoft.com/en-us/azure/iot-hub-device-update/deploy-update).
Once you have deployed the update, the application should start downloading the upload in a few seconds and install the
update once downloaded.

---
**Note**

Once the device has received the update, the cloud service records the device's status as "update in progress",
and later as "on latest update" if the installation is successful. There is no way to reset the cloud's record of
the device to an initial, "not updated" state.

Therefore, if you run the example a second time, the cloud may not push
the update to the same device. In this case, you may need to delete the device instance from IoT Hub and create a new
device, as devices are created in a clean state.

As new devices are created with new connection credentials, you will need to rebuild the application and import the update to Azure IoT Hub again. To avoid doing that do the following:

1. Copy the current device primary and secondary keys from Azure IoT Hub
1. Delete the previously created device
1. Add a new device with the same name as the previous device
1. Ensure the *Auto-generate keys* checkbox is unticked to paste the primary and secondary keys previously copied
1. Click the *Save* button to complete the creation of the new device

---

## Expected behaviours
The application should print out information similar to the content of [`test.log`](test.log) when successful.

## Additional FVP parameters

This example requires a network interface. To enable it on an MPS3-based FVP such as Corstone-300 AN552,
pass the following additional arguments to the FVP:

```
-C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1
```

E.g.
```
FVP_Corstone_SSE-300_Ethos-U55 -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1 -a __build/iotsdk-example-azure-netxduo-device-update_merged.elf
```


## Building the example

### Prerequisites

Please refer to [prerequisites for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/Prerequisites.md).

### Background

#### CMake build process

Building a CMake project involves two steps:
* Configuration: What to configure depends on the project. For the Open IoT SDK,
this includes build directory, toolchain selection, components to fetch, options
for examples and tests, etc. This step creates and populates the build directory
with everything needed for building, including automatically fetching components.
* Building: Triggering a build using the configuration set up in the
Configuration step.

#### Selecting the toolchain

To configure CMake to use a particular toolchain you must pass in a
[toolchain file](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html#cross-compiling).

The example will automatically fetch the default toolchain files for
* Arm Compiler 6: `--toolchain toolchains/toolchain-armclang.cmake`
* GNU Arm Embedded Toolchain (GCC) `--toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake`

### Configuration and building

To configure this example, e.g. using GCC:

```
cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake
```

**Notes**:
* If you generated this example from a copy of the Open IoT SDK that contains *your own modifications*, then you
**must** append `-D FETCHCONTENT_SOURCE_DIR_OPEN-IOT-SDK=<YOUR_LOCAL_SDK>` (note the difference between `_` and `-`) to
the command above, where `<YOUR_LOCAL_SDK>` is the absolute or relative path to the SDK repository located on your hard
drive.

To build:

```
cmake --build __build
```

## Running the example

Once the example application has been built, it can be found in .elf and .bin formats inside the `__build` directory.

For information on setting up Arm Virtual Hardware through AWS on an [Amazon Machine Image] (AMI), please see [here][Setting Up AVH].

### To run it on an Arm Virtual Hardware (AVH) target:



### To run it on a Fixed Virtual Platform (FVP) target:





## Troubleshooting

### Windows path issue

When running the above `cmake -B` command on Windows, you may run into the following issue:

```
ninja: error: Stat(__build/_deps/a-very-very-very-very-very-long-component-name-subbuild/a-very-very-very-very-very-long-component-name-populate-prefix/src/a-very-very-very-very-very-long-component-name-populate-stamp/a-very-very-very-very-very-long-component-name-populate-done): Filename longer than 260 characters
```

You can work around the error by performing either of the following solutions:
- using WSL
- moving the project closer to the drive's root
- using `subst` windows command

For example `subst` to use new drive `z:` if the path to your working directory is `c:\foo\very-very-long-directory-name\my-working-directory`,
run the commands as follows:
```
subst z: c:\foo\very-very-long-directory-name\my-working-directory
z:
git config --system core.longpaths true
```

## Further information

Please refer to [documentation for the Open IoT SDK](https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/README.md).

[Setting Up AVH]: https://git.gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/docs/running_apps_on_avh.md
[Amazon Machine Image]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html
