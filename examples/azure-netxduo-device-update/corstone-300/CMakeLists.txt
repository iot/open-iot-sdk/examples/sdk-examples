# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.21.0 FATAL_ERROR)

# Set config variables for the project and dependencies
# Note: NetX Duo configuration variables are set as CACHE variables, because
# NetX Duo calls cmake_minimum_required() with a very old version of CMake that
# does not allow overriding CACHE configuration variables with regular non-CACHE
# ones.
set(CMAKE_SYSTEM_PROCESSOR cortex-m55)
set(IOTSDK_FETCH_LIST
    netxduo
    create-adu-import-manifest
    trusted-firmware-m
    threadx
)
set(CMSIS_PACK_PLATFORM_DEVICE_NAME   "SSE-300-MPS3")
set(NX_USER_FILE "${CMAKE_CURRENT_LIST_DIR}/netxduo-config/nx_user.h" CACHE STRING "")
set(NXD_ENABLE_FILE_SERVERS OFF CACHE BOOL "")
set(NXD_ENABLE_AZURE_IOT ON CACHE BOOL "")
set(NXD_ENABLE_AZURE_IOT_ADU_PSA ON CACHE BOOL "")
set(NXD_AZURE_IOT_PSA_LIBRARY tfm-ns-interface CACHE STRING "")
# Note: -DMCUBOOT_LOG_LEVEL=INFO enables the bootloader log which shows the installation process of the update
set(TFM_CMAKE_ARGS
    -D TFM_PLATFORM=arm/mps3/an552
    -D TFM_PROFILE=profile_medium
    -D CONFIG_TFM_ENABLE_CP10CP11=ON
    -D TFM_PARTITION_FIRMWARE_UPDATE=ON
    -D PLATFORM_HAS_FIRMWARE_UPDATE_SUPPORT=ON
    -D MCUBOOT_DATA_SHARING=ON
    -D MCUBOOT_LOG_LEVEL=INFO
)
set(MCUBOOT_IMAGE_VERSION_NS "0.0.1")

set(TX_USER_FILE "${CMAKE_CURRENT_LIST_DIR}/threadx-config/tx_user.h")

include(FetchContent)

# Toolchain files need to exist before first call to project
FetchContent_Declare(iotsdk-toolchains
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/toolchain.git
    GIT_TAG         d600b9dc9aca55c8428b63b9b8eff1ff97743382
    SOURCE_DIR      ${CMAKE_BINARY_DIR}/toolchains
)
FetchContent_MakeAvailable(iotsdk-toolchains)

# Declare top-level project
project(iotsdk-example-azure-netxduo-device-update)

# Set executable suffix to be toolchain-independent for ease of documentation
# and testing. This needs to be done after first call to project().
set(CMAKE_EXECUTABLE_SUFFIX .elf)

# Add Open IoT SDK
FetchContent_Declare(open-iot-sdk
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git
    GIT_TAG         3829c52523d9fddc0af1e097e16e772377075b99

)
FetchContent_MakeAvailable(open-iot-sdk)

# Add Open IoT SDK modules to path
list(APPEND CMAKE_MODULE_PATH ${open-iot-sdk_SOURCE_DIR}/cmake)
list(APPEND CMAKE_MODULE_PATH ${open-iot-sdk_SOURCE_DIR}/components/trusted-firmware-m)

# Add the project_options tool for code quality checks on this example.
# In a user's own project, the use of project_options is entirely optional.
FetchContent_Declare(project_options
    GIT_REPOSITORY  https://github.com/cpp-best-practices/project_options.git
    GIT_TAG         v0.21.0
)
FetchContent_MakeAvailable(project_options)
include(${project_options_SOURCE_DIR}/Index.cmake)
include(${project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

# Apply Open IoT SDK's default compiler warnings to this example via project_options
include(${open-iot-sdk_SOURCE_DIR}/cmake/AddToolchainWarningFlags.cmake)
set(COMPILER_WARNINGS ${IOTSDK_WARNING_FLAGS} ${IOTSDK_WARNING_AS_ERRORS_FLAGS})
dynamic_project_options(
    CLANG_WARNINGS  ${COMPILER_WARNINGS}
    GCC_WARNINGS    ${COMPILER_WARNINGS}
)

include(ConvertElfToBin)
include(SignTfmImage)
include(CTest)

include(cmsis-pack-platform.cmake)

# For simplicity, extract Device ID from the credentials header and use it as
# a unique ID for everything in steps below.
file(STRINGS ${CMAKE_CURRENT_LIST_DIR}/sample_azure_iot_credentials.h unique_id
     REGEX "define *DEVICE_ID" LIMIT_COUNT 1)
string(REGEX MATCH "\".*\"" unique_id "${unique_id}")
# Remove quotation marks
string(REGEX MATCH "[a-zA-Z0-9_\-]+" unique_id "${unique_id}")
if("${unique_id}" STREQUAL "")
    message(FATAL_ERROR "Failed to extract Azure IoT Hub Device ID from sample_azure_iot_credentials.h")
endif()

# Add example application
add_executable(iotsdk-example-azure-netxduo-device-update
    main.c
    sample_azure_iot_embedded_sdk.c
    nx_azure_iot_cert.c
    nx_azure_iot_ciphersuites.c
)

add_dependencies(iotsdk-example-azure-netxduo-device-update trusted-firmware-m-build)

target_link_options(iotsdk-example-azure-netxduo-device-update
    PUBLIC
        # Use newlib-nano instead of full libc to reduce code size and fit the
        # application into the non-secure ROM
        $<$<STREQUAL:${CMAKE_C_COMPILER_ID},GNU>:--specs=nano.specs>
)

target_compile_definitions(iotsdk-example-azure-netxduo-device-update
    PRIVATE
        SAMPLE_MANUFACTURER="Arm-Ltd-${unique_id}"
        SAMPLE_MODEL="Corstone-300"
)

target_link_libraries(tfm-ns-interface PRIVATE tfm-ns-interface-threadx)

# Link serial retargeting with a device-specific UART CMSIS-Driver.
target_link_libraries(iotsdk-serial-retarget PRIVATE cmsis-pack-platform)

target_link_libraries(iotsdk-example-azure-netxduo-device-update
    nx-ip-link-cmsis-eth
    netxduo-adu-driver-psa

    iotsdk-serial-retarget

    tfm-ns-interface-threadx

    threadx
    cmsis-pack-platform
)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/gcc.ld)
    target_link_options(iotsdk-example-azure-netxduo-device-update
        PRIVATE
            -T ${linker_script}
            -Wl,--gc-sections
    )
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/armclang.sct)
    target_link_options(iotsdk-example-azure-netxduo-device-update PRIVATE --scatter=${linker_script})
endif()

set_target_properties(iotsdk-example-azure-netxduo-device-update PROPERTIES LINK_DEPENDS ${linker_script})

# Sign the initial image with version 0.0.1
iotsdk_tf_m_sign_image(iotsdk-example-azure-netxduo-device-update 0.0.1)

# A user project that consumes the SDK needs to explicitly provide
# addresses in order to merge images for TF-M. The addresses cannot
# be easily programmatically extracted as they are defined in linker
# scripts.
# Order: <bootloader> <signed secure TF-M firmware> <signed non-secure user app>
iotsdk_tf_m_merge_images(iotsdk-example-azure-netxduo-device-update 0x10000000 0x11000000 0x01060000)

# Create an device update image, using the same application but signing as version 0.0.2
iotsdk_tf_m_sign_image(iotsdk-example-azure-netxduo-device-update 0.0.2)

# Copy the device update image as update_image.bin
add_custom_command(
    TARGET
        iotsdk-example-azure-netxduo-device-update
    POST_BUILD
    DEPENDS
        iotsdk-example-azure-netxduo-device-update_signed.bin
    BYPRODUCTS
        update_image.bin
    COMMAND
        ${CMAKE_COMMAND} -E copy iotsdk-example-azure-netxduo-device-update_signed.bin update_image.bin
    COMMAND
        ${CMAKE_COMMAND} -E echo "-- Update image: ${CMAKE_CURRENT_BINARY_DIR}/update_image.bin"
    VERBATIM
)

# Create import manifest.
FetchContent_GetProperties(create-adu-import-manifest)
add_custom_command(
    TARGET
        iotsdk-example-azure-netxduo-device-update
    POST_BUILD
    DEPENDS
        update_image.bin
    BYPRODUCTS
        update-0.0.2.importmanifest.json
    COMMAND
        bash ${create-adu-import-manifest_SOURCE_DIR}/create-adu-import-manifest.sh
        -p Arm-Ltd -n ${unique_id} -v 0.0.2 -c manufacturer:Arm-Ltd-${unique_id}
        -c model:Corstone-300 -h microsoft/swupdate:1
        -r installedCriteria:1.0 update_image.bin
        > update-0.0.2.importmanifest.json
    COMMAND
        ${CMAKE_COMMAND} -E echo "-- Import manifest: ${CMAKE_CURRENT_BINARY_DIR}/update-0.0.2.importmanifest.json"
    VERBATIM
)

## Automated testing

# Step 1
# Connect the device to the cloud once, so that the Device Update service
# enumerates this device and its group.
add_test(
    NAME    initial-connection
    COMMAND htrun
        --image-path=iotsdk-example-azure-netxduo-device-update_merged.elf
        --compare-log=${CMAKE_CURRENT_LIST_DIR}/initial_connection.log
        --sync=0 --baud-rate=115200 --polling-timeout=240
        --micro=FVP_CS300_U55 --fm=MPS3
    COMMAND_EXPAND_LISTS
)

# Step 2
# Import the update manifest and the image to the Device Update service.
# This takes a few minutes.
add_test(
    NAME    import-update
    COMMAND python3 ${open-iot-sdk_SOURCE_DIR}/developer-tools/utils/cloud_helper/azure_device_update.py
        import --manifest ${CMAKE_BINARY_DIR}/update-0.0.2.importmanifest.json
        --payload ${CMAKE_BINARY_DIR}/update_image.bin
        --directory ${unique_id}
    COMMAND_EXPAND_LISTS
)

# Step 3
# Deploy the image we have just imported, to the group containing the device.
add_test(
    NAME    deploy-update
    COMMAND python3 ${open-iot-sdk_SOURCE_DIR}/developer-tools/utils/cloud_helper/azure_device_update.py
        deploy --provider Arm-Ltd --update ${unique_id} --version 0.0.2 --group ${unique_id} --deployment-id ${unique_id}
    COMMAND_EXPAND_LISTS
)

# Step 4
# Connect the device. It should receive and install the update.
add_test(
    NAME    connection-and-device-update
    COMMAND htrun
        --image-path=iotsdk-example-azure-netxduo-device-update_merged.elf
        --compare-log=${CMAKE_CURRENT_LIST_DIR}/device_update.log
        --sync=0 --baud-rate=115200 --polling-timeout=240
        --micro=FVP_CS300_U55 --fm=MPS3
    COMMAND_EXPAND_LISTS
)

# Step 5
# Clean up CI resources.
add_test(
    NAME    cleanup
    COMMAND python3 ${open-iot-sdk_SOURCE_DIR}/developer-tools/utils/cloud_helper/azure_device_update.py
        cleanup
        --group ${unique_id}
        --provider Arm-Ltd
        --update ${unique_id}
        --version 0.0.2
        --directory ${unique_id}
    COMMAND_EXPAND_LISTS
)
