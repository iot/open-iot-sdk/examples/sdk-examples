/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef TX_USER_H
#define TX_USER_H

/* Run ThreadX on the non-secure side only, when TrustZone is configured */
#define TX_SINGLE_MODE_NON_SECURE

#endif
