/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef IOTSDK_ASAN_CONFIG_H
#define IOTSDK_ASAN_CONFIG_H

#include <pw_log/log.h>
#define iotsdk_sanitizers_error_cb(...) PW_LOG_ERROR(__VA_ARGS__)

#endif /* ! IOTSDK_ASAN_CONFIG_H */
