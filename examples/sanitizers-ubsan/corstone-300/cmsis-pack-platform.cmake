# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

set(CMSIS_PACKS
"ARM::V2M_MPS3_SSE_300_BSP@1.3.0"
"ARM::CMSIS@5.9.0"
"ARM::CMSIS-Driver@2.7.2"
)

set(CMSIS_PACK_COMPONENTS
"ARM::Device:Definition@1.2.0"
"ARM::Device:Startup&Baremetal@1.2.0"
"ARM::Native Driver:Timeout@1.0.0"
"ARM::Native Driver:SysCounter@1.1.0"
"ARM::Native Driver:SysTimer@1.1.0"
"ARM::Native Driver:Flash@1.0.0"
"ARM::CMSIS Driver:Flash:SRAM@1.1.0"
"ARM::CMSIS Driver:USART@1.0.0"
"ARM::Native Driver:UART@1.1.0"
"CMSIS Driver:Ethernet:ETH_LAN91C111"
)

add_cmsis_library(
  cmsis-pack-platform                  # library target name
  ${CMSIS_PACK_PLATFORM_DEVICE_NAME}   # device
  AC6                                  # compiler
                                       # Note: some packs assume it will only be used with Armclang compiler and set it
                                       # as requirement. Our examples do not use compiler dependent parts from the packs
  CMSIS_PACKS                          # list of packs
  CMSIS_PACK_COMPONENTS                # list of components
)

FetchContent_MakeAvailable(cmsis-5)

target_include_directories(iotsdk-cmsis-core-device
    INTERFACE
        ${cmsis-5_SOURCE_DIR}/CMSIS/Core/Include
        $<$<STREQUAL:${CMAKE_SYSTEM_PROCESSOR},cortex-m55>:${cmsis-5_SOURCE_DIR}/Device/ARM/ARMCM55/Include>
        $<$<STREQUAL:${CMAKE_SYSTEM_PROCESSOR},cortex-m85>:${cmsis-5_SOURCE_DIR}/Device/ARM/ARMCM85/Include>
        $<$<STREQUAL:${CMAKE_SYSTEM_PROCESSOR},cortex-m4>:${cmsis-5_SOURCE_DIR}/Device/ARM/ARMCM4/Include>
)

# CMSIS 5 require projects to provide configuration macros via RTE_Components.h
# and CMSIS_device_header. The macro CMSIS_device_header is not automatically set
# based on CMAKE_SYSTEM_PROCESSOR in the place where cmsis-core is first defined,
# because a project may want to provide its own device header.
target_compile_definitions(iotsdk-cmsis-core-device INTERFACE CMSIS_device_header="SSE300MPS3.h")

# The cmsis-pack-platform contains the system specific header files so it has to be linked to cmsis-core.
target_link_libraries(iotsdk-cmsis-core-device INTERFACE cmsis-pack-platform)

# The startup files that are located in the device CMSIS Pack depends on CMSIS-Core.
# The CMSIS-Core Component is not added by the cmsis-pack-utils because it is used as an sdk-component.
target_link_libraries(cmsis-pack-platform
    PUBLIC
        iotsdk-cmsis-core-device
)

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    # for ETH_LAN91C111 Driver
    target_compile_definitions(cmsis-pack-platform
        PRIVATE
            __rbit=__RBIT
    )
endif()
