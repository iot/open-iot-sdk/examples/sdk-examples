# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.21.0 FATAL_ERROR)

# Set config variables for the project and dependencies
set(CMAKE_SYSTEM_PROCESSOR cortex-m55)
set(IOTSDK_FETCH_LIST
    cmsis-5
    pigweed
)
set(CMSIS_PACK_PLATFORM_DEVICE_NAME   "SSE-300-MPS3")

include(FetchContent)

# Toolchain files need to exist before first call to project
FetchContent_Declare(iotsdk-toolchains
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/toolchain.git
    GIT_TAG         d600b9dc9aca55c8428b63b9b8eff1ff97743382
    SOURCE_DIR      ${CMAKE_BINARY_DIR}/toolchains
)
FetchContent_MakeAvailable(iotsdk-toolchains)

# Declare top-level project
project(iotsdk-example-sanitizers-ubsan)

# Set executable suffix to be toolchain-independent for ease of documentation
# and testing. This needs to be done after first call to project().
set(CMAKE_EXECUTABLE_SUFFIX .elf)

# Set Pigweed backends before SDK fetch.
function(set_pw_backend FACADE BACKEND)
    set(${FACADE}_BACKEND ${BACKEND} CACHE STRING "" FORCE)
endfunction()
set_pw_backend(pw_log pw_log_cmsis_driver)

# Add Open IoT SDK
FetchContent_Declare(open-iot-sdk
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git
    GIT_TAG         3829c52523d9fddc0af1e097e16e772377075b99

)
FetchContent_MakeAvailable(open-iot-sdk)

# Add Open IoT SDK modules to path
list(APPEND CMAKE_MODULE_PATH ${open-iot-sdk_SOURCE_DIR}/cmake)

# Add the project_options tool for code quality checks on this example.
# In a user's own project, the use of project_options is entirely optional.
FetchContent_Declare(project_options
    GIT_REPOSITORY  https://github.com/cpp-best-practices/project_options.git
    GIT_TAG         v0.21.0
)
FetchContent_MakeAvailable(project_options)
include(${project_options_SOURCE_DIR}/Index.cmake)
include(${project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

# Apply Open IoT SDK's default compiler warnings to this example via project_options
include(${open-iot-sdk_SOURCE_DIR}/cmake/AddToolchainWarningFlags.cmake)
set(COMPILER_WARNINGS ${IOTSDK_WARNING_FLAGS} ${IOTSDK_WARNING_AS_ERRORS_FLAGS})
dynamic_project_options(
    CLANG_WARNINGS  ${COMPILER_WARNINGS}
    GCC_WARNINGS    ${COMPILER_WARNINGS}
)

include(ConvertElfToBin)
include(CTest)

include(cmsis-pack-platform.cmake)

# Make sure ubsan can find its config file & callbacks.
target_include_directories(iotsdk-sanitizers-common INTERFACE ./sanitizers-config)
target_link_libraries(iotsdk-sanitizers-common INTERFACE pw_log)

# Link serial retargeting with a device-specific UART CMSIS-Driver.
target_link_libraries(iotsdk-serial-retarget PRIVATE cmsis-pack-platform)

# Add example application
add_executable(iotsdk-example-sanitizers-ubsan
    main.c
    test.c
)

target_link_libraries(iotsdk-example-sanitizers-ubsan
    pw_log
    pw_log_cmsis_driver
    pw_log_null_lock

    iotsdk-serial-retarget

    iotsdk-sanitizers-ubsan

    cmsis-pack-platform

    # SDK project options/warnings (this is optional)
    project_options
    project_warnings
)


if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/gcc.ld)
    target_link_options(iotsdk-example-sanitizers-ubsan PRIVATE -T ${linker_script})
elseif(CMAKE_C_COMPILER_ID STREQUAL "ARMClang")
    set(linker_script ${CMAKE_CURRENT_LIST_DIR}/armclang.sct)
    target_link_options(iotsdk-example-sanitizers-ubsan PRIVATE --scatter=${linker_script})
endif()
set_target_properties(iotsdk-example-sanitizers-ubsan PROPERTIES LINK_DEPENDS ${linker_script})
target_elf_to_bin(iotsdk-example-sanitizers-ubsan)

add_test(
    NAME    iotsdk-example-sanitizers-ubsan
    COMMAND htrun
        --image-path=iotsdk-example-sanitizers-ubsan.elf
        --compare-log=${CMAKE_CURRENT_LIST_DIR}/test.log
        --sync=0 --baud-rate=115200 --polling-timeout=240
        --micro=FVP_CS300_U55 --fm=MPS3
    COMMAND_EXPAND_LISTS
)
