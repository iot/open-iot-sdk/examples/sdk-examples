## Contributing

This repository is only updated by an automation bot to regularly synchronize examples generated from the latest Open
IoT SDK. If you would like to contribute to any examples, please add your changes to templates in [the Open IoT SDK](
https://gitlab.arm.com/iot/open-iot-sdk/sdk) and raise a merge request there.
