# v2023.04 (2023-04-13)

## Changes

* ci: Enable possible tpip violation warning messages in Merge Requests.


# v2023.01 (2023-01-19)

## Changes

* ci: Add towncrier news fragments and configuration
* ci: Enable autobot and update developer-tools
